#Variable declarations
CC = g++
CPPX = cpp
OBJX = o
TXTX = txt
BIN_DIR = bin
SRC_DIR = src
OBJFLAG = -o
NLFLAG = -c #no link flag
CFLAGS = -Wextra -Wall -Werror -pedantic

BIN = wdcount

# $(patsubst "find this pattern", "replace it with this", "use these files")
OBJS := $(patsubst %.$(CPPX), %.$(OBJX), $(wildcard $(SRC_DIR)/*.$(CPPX)))
TXTS := $(wildcard *.$(TXTX))

#explicit dependency rules
%.$(OBJX): %.$(CCPX)
	@echo "creating objects..."
	$(CC) $(CLFAGS) $(NLFLAG) $(OBJFLAG) $@ $^
	@echo ""

#install text file function
# param 1: text file name
# param 2: text file parent dir
define install_file

$(eval FILE_EXIST := @echo "file exists. Moving on...")
$(eval FILE_INSTALL := install -m644 $(1) $(2))

$(if $(wildcard $(2)/$(1)),$(FILE_EXIST),$(FILE_INSTALL))

endef

#Main Target
$(BIN): $(OBJS)
	@echo "creating local binary directory..."
	install -d $(BIN_DIR)
	@echo ""
	@echo "creating user binary directory..."
	install -d ~/$(BIN_DIR)
	@echo ""
	@echo "creating user files directory..."
	install -d ~/$@
	@echo ""
	@echo "linking..."
	$(CC) $(CLFLAGS) $(OBJFLAG) $(BIN_DIR)/$@ $^
	@echo ""
	@echo "installing binary to user binary directory..."
	install $(BIN_DIR)/$@ ~/$(BIN_DIR)
	@echo ""
	@echo "installing additional files to user directory..."
	$(foreach txt,$(TXTS),$(call install_file,$(txt),~/$@))
	@echo ""
	@echo "installing complete. Exiting..."

#clean Standard Target
clean:
	@echo "removing user binary directory and user files directory..."
	rm -rf ~/$(BIN_DIR)/$(BIN) ~/$(BIN)
	@echo "removing local binary directory and object files..."
	rm -rf $(BIN_DIR) $(SRC_DIR)/*.$(OBJX)
	@echo "cleaning complete. Exiting..."