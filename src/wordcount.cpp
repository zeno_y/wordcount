#include "wordcount.hpp"

#define DIVIDER ' '

//char array of punctuations to remove.
const char PUNCT[] = {
    '!', '?', '.', ',', '\'', '\"', '(', ')', ';', ':'
};

//char array of possible word dividers.
const char DIVS[] = {
    ' ', '-', '/'
};

//char pointers used to print to console in color.
const char * COLOR_RESET = "\033[0m";
const char * COLOR_RED = "\033[31m";
const char * COLOR_GREEN = "\033[32m";
const char * COLOR_YELLOW = "\033[33m";

//operator < overload for the wd_count class. Compares word counts.
bool wordcount::wd_count::operator < (const wd_count & rhs) const {
    return this->get_count() < rhs.get_count();
}

//operator == overload for the wd_count class. Compares words.
bool wordcount::wd_count:: operator == (const wd_count & rhs) const {
    return this->get_word().compare(rhs.get_word()) == 0;
}

//operator ++ overload for wd_count class. Pre-increments word count.
wordcount::wd_count & wordcount::wd_count::operator ++ () {
    ++_count;
    return *this;
}

/**
 * Retrieves words to exclude from the exclusions file.
 * * file is of type std::ifstream and is the file stream containing words to exclude.
 * ** return is of reference type std::vector<std::string>
*/
std::vector<std::string> wordcount::get_exclusions(std::ifstream & file) {
    std::vector<std::string> exclusions{};
    std::string line{};

    while(std::getline(file, line)) exclusions.push_back(line);

    return exclusions;
}

/**
 * Prints excluded words in one line.
 * * exclusions is of const reference type std::vector<std::string> and is the list of words to exclude.
*/
void wordcount::print_exclusions(const std::vector<std::string> & exclusions) {
    for(unsigned int i = 0; i < exclusions.size(); ++i) {
        std::cout << exclusions.at(i);
        if(i < exclusions.size() - 1) std::cout << ' ';
    }

    std::cout << '\n' << std::endl;
}

/**
 * Adds word to exclude to the exclusion vector.
 * 
 * * exclusions is of reference type std::vector<std::string> and is the vector to add the exlusion word to.
 * * word is of reference type std::string and is the word to add to the exclusion vector.
 * ** return is of type boolean and is true if word was added to exclusions vector.
*/
bool wordcount::add_exclusion(std::vector<std::string> & exclusions, std::string & word) {
    for(const std::string w : exclusions) {
        if(w.compare(word) == 0) return false;
    }

    //appends word to word vector
    exclusions.push_back(word);
    return true;
}

/**
 * Removes word to exclude from the exclusion vector.
 * 
 * * exclusions is of reference type std::vector<std::string> and is the vector to remove the exclusion word from.
 * * word is of type std::string and is the word to remove from the exclusion vector.
 * ** return is of type boolean and is true if word was removed from exclusions vector.
*/
bool wordcount::remove_exclusion(std::vector<std::string> & exclusions, std::string & word) {
    //auto should evalute to std::vector::iterator
    for(auto it = exclusions.begin(); it != exclusions.end(); ++it) {
        if((*it).compare(word) == 0) {
            exclusions.erase(it);
            return true;
        }
    }

    return false;
}

/**
 * Adds words to file.
 * 
 * * file is of reference type std::ofstream and is the file to add the exclusions to. It is assumed
 *   file was opened with the 'truncated' flag.
 * * exclusions is of reference type std::vector<std::string> and is the list of exclusions to add to the file.
*/
void wordcount::renew_exclusion(std::ofstream & file, std::vector<std::string> & exclusions) {
    for(const std::string & ex : exclusions) {
        file << ex;
        file << '\n';
    }
}

/**
 * Adds words not encountered in current text. If word was already encountered, increments word counter.
 * 
 * * words_v is of reference type std::vector<wordcount::wd_count> and is the vector containing wd_count objects.
 * * word is of const reference type std::String and is the word to add to the words vector.
*/
void add_word(std::vector<wordcount::wd_count> & words_v, const std::string & word) {
    bool word_found = false;

    //w must be a reference to properly change the data within words_v's elements
    for(wordcount::wd_count & w : words_v) {
        //Word found in vector; increments count
        if(word.compare(w.get_word()) == 0) {
            ++w;
            word_found = true;  
            break;
        }
    }

    //Adds word that didn't exist in vector prior.
    if(!word_found) {
        wordcount::wd_count wordcount{word};
        words_v.push_back(wordcount);
    }
}

/**
 * Excludes words that match the predefined exclusion list
 * 
 * * exclusions is of const reference type std::vector<std::String> and is the list of words to exclude
 * * word is of const reference type std::string and is the word to check whether exclusion is necessary
 * ** return is of type boolean and is true if word needs to be excluded.
*/
bool exclude_word(const std::vector<std::string> & exclusions, const std::string & word) {
    //excludes short and empty words
    if(word.length() <= 3) return true;

    //excludes words matching exclusions list
    for(const std::string & e : exclusions) if(e.compare(word) == 0) return true;

    //Returns false if prior "return true" conditions aren't met
    return false;
}

/**
 * Sets all letters in a word to lowercase and remove punctuations predefined in
 * wordcount::PUNCT[].
 * 
 * * word is of reference type std::string and is the word to format. Not const since
 *   string is altered here.
*/
void format_word(std::string & word) {
    //Coverts all words characters into lowercase.
    std::transform(word.begin(), word.end(), word.begin(), tolower);

    int reduce_by{0};
    //Loops through every character in word, comparing it to
    // every character in PUNCT[]
    //auto should evaluate to std::string::iterator
    for(auto it = word.begin(); it != word.end(); ++it) {
        for(const char & p : PUNCT) {
            if(p == *it) {
                ++reduce_by;
                //Shifts all subsequent letters in the word to the left by one.
                // Loops until one character prior to string end.
                // If prior IF statement is handling the last character
                // in the string, the elimination of it is handled by the string::resize()
                // statement at the end of this function.
                for(auto it_adj = it; it_adj != --word.end(); ++it_adj) {
                    *it_adj = *(it_adj + 1);
                }

                //breaks out of current for loop since only one PUNCT
                // character can occupy this string character space
                break;
            }
        }
    }

    //Resizes the string 
    if(reduce_by > 0) word.resize(word.size() - reduce_by);

    // if(word.size() > 0 && word.at(word.size() - 1) == 's') word.resize(word.size() - 1);
}

/**
 * Determines the end of the next word, delimited by any one of the chars defined in wordcount::DIVS[]
 * 
 * * line is of const reference type std::string and is one line in the text file.
 * * start_pos is of unsigned type int and is the starting position for finding word breaks.
 * ** return is of type std::size_t and is the end position of the next word.
*/
std::size_t end_of_word(const std::string & line, unsigned int start_pos) {
    //sets to max to ensure code block below finds next smallest word available
    std::size_t end{std::numeric_limits<std::size_t>::max()};

    //Finds the next shortest word available
    for(const char & d : DIVS) end = std::min(end, line.find(d, start_pos));

    //Returns end position
    return end;
}

/**
 * Counts word in provided file, excluding certain predefined words. Creates pointers with NEW!
 * 
 * * file is of reference type ifstream and is the file stream containing the words to sort and count.
 * * exclusions is of const reference type std::vector<std::string> and is the list of exclusion words to not process.
 * ** return is of reference type std::vector<wordcount::wd_count> and is the vector of wd_count objects found in the text file.
*/
std::vector<wordcount::wd_count> wordcount::count_words(std::ifstream & file, const std::vector<std::string> & exclusions) {
    std::vector<wordcount::wd_count> words_v{};
    words_v.clear();

    std::string line{};

    //Loop through all lines
    while(std::getline(file, line)) {
        std::size_t start{0};
        std::size_t end{end_of_word(line, start)};
        //Loop through all words in one line
        bool keep_looking{true};
        while(keep_looking) {
            //Retrieves word substring from line.
            // If end variable couldn't find the end of the next word,
            // it is assumed we have reached the end of the line and uses a different string::substr() overload
            std::string word = end != std::string::npos ? line.substr(start, end - start) : line.substr(start);

            //Formats words
            format_word(word);
            //If word is not in the exlusion lost, adds to word vector
            if(!exclude_word(exclusions, word)) add_word(words_v, word);

            //Determines the start/end positions of the next word
            start = end;
            if(start != std::string::npos) {
                //Moves word start by one position past the previous word end
                ++start;
                //Determines word end for the next word
                end = end_of_word(line, start);
            } else {
                keep_looking = false;
            }
        }
    }

    return words_v;
}

/**
 * Sorts words from lowest to highest.
 * Function std::sort() uses overloaded operator < in wd_count.
 * 
 * * words_v is of reference type std::vector<wordcount::wd_count> and is the words to sort.
 *   Not const since vector is sorted here. A reference since calling function wants the passed
 *   vector sorted.
*/
void wordcount::sort_words(std::vector<wordcount::wd_count> & words_v) {
    std::sort(words_v.begin(), words_v.end());
}

/**
 * Prints words
 * 
 * * words_v is of const reference type std::vector<wordcount::wd_count> and is the words to print.
*/
void wordcount::print_words(const std::vector<wordcount::wd_count> & words_v, const std::vector<wordcount::wd_count> & basewords_v) {
    if(words_v.empty()) {
        std::cout << "No words to show." << std::endl;
        return;
    }

    //Remember to keep element objects like below
    // const references to prevent the copy constructor from firing.
    for(const wd_count & w : words_v) {
        if(!basewords_v.empty()) {
            int word_count{0};

            for(const wd_count & bw : basewords_v) {
                if(w == bw) {
                    word_count = bw.get_count();
                    break;
                }
            }

            //Color rules
            // leave basewords_v with count 0 AND words_v with count 1 as white
            // basewords_v with count 0 AND words_v with count > 1 is red
            // basewords_v with count > 0 AND words_v with count > 1 is yellow
            // basewords_v with count >= to words_v count is green

            if(w.get_count() > 1) {
                if(word_count == 0) std::cout << COLOR_RED;
                else if(word_count < w.get_count()) std::cout << COLOR_YELLOW;
            }
            
            if(word_count >= w.get_count()) std::cout << COLOR_GREEN;

            std::cout << word_count << '/';
        }
        
        std::cout << w.get_count() << " - " << w.get_word() << COLOR_RESET << std::endl;
    }
}