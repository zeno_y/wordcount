#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include "wordcount.hpp"
#include "usage.hpp"

#define EXCL_MANIP_ADD 1
#define EXCL_MANIP_RM 2

#define TO_PRINT true

//Retrieves home directory
const std::string get_home() {
    char * home = getenv("HOME");
    if(home == NULL) home = getpwuid(getuid())->pw_dir;

    std::string home_str{home};
    return home_str; //returns copy
}

//These file names should match files created in Make.
const std::string HOME = get_home();
const std::string FILE_WORDS{"/wdcount/words.txt"};
const std::string FILE_BASEWORDS{"/wdcount/basewords.txt"};
const std::string FILE_EXCLUSIONS{"/wdcount/exclusions.txt"};

/**
 * Prints message to the console.
 * * msg is of const type char * and is the message to print out.
*/
void print_msg(const char * msg) {
    std::cout << msg << std::endl;
}

/**
 * Main function.
 * 
 * Handles program arguments, opening & closing files, retrieving words from text and
 * sending it off to print.
*/
int main(int argc, char * argv[]) {
    //find exclusions here
    std::ifstream i_file_exclusions;
    i_file_exclusions.open(HOME + FILE_EXCLUSIONS);

    std::vector<std::string> exclusions{};
    if(i_file_exclusions.is_open()) {
        exclusions = wordcount::get_exclusions(i_file_exclusions);
        i_file_exclusions.close();
    } else {
        print_msg("Couldn't open exclusions file; no words to exclude were found. Exiting...\n");
        return EXIT_FAILURE;
    }

    std::vector<wordcount::wd_count> basewords_v{};
    // int flag_cmp{0};
    //Handles arguments. First arg is the program bin name.
    if(argc > 1) {
        //Should only run once.
        // Used while loop to allow to "break" out.
        for(int i = ARG_POS; i < argc; ++i) {
            if(strncmp(ARG_HELP, argv[i], ARG_SIZE) == 0) {
                usage::print_usage();
                return EXIT_SUCCESS;
            }

            if(strncmp(ARG_LIST, argv[i], ARG_SIZE) == 0) {
                print_msg("\nWord(s) being excluded:");
                wordcount::print_exclusions(exclusions);
                return EXIT_SUCCESS;
            }

            if(strncmp(ARG_CMP, argv[i], ARG_SIZE) == 0) {
                std::ifstream file_base;
                file_base.open(HOME + FILE_BASEWORDS);

                if(file_base.is_open()) {
                    basewords_v = wordcount::count_words(file_base, exclusions);
                    file_base.close();
                    break;
                } else {
                    print_msg("Couldn't open base file; can't compare. Exiting...");
                    return EXIT_FAILURE;
                }
            }

            //Handles exclusion manipulation arguments.
            // Converts arguments into numerical values for switch statement use.
            int excl_manip{0};
            if(strncmp(ARG_ADD, argv[i], ARG_SIZE) == 0) excl_manip = EXCL_MANIP_ADD;
            else if(strncmp(ARG_REMOVE, argv[i], ARG_SIZE) == 0) excl_manip = EXCL_MANIP_RM;
            
            if(excl_manip > 0) {
                std::vector<std::string> unused{};

                switch(excl_manip) {
                    case EXCL_MANIP_ADD:
                        for(int j = i + 1; j < argc; ++j) {
                            std::string word{argv[j]};
                            if(!wordcount::add_exclusion(exclusions, word)) unused.push_back(word);
                        }

                        break;
                    case EXCL_MANIP_RM:
                        for(int j = i + 1; j < argc; ++j) {
                            std::string word{argv[j]};
                            if(!wordcount::remove_exclusion(exclusions, word)) unused.push_back(word);
                        }

                        break;
                }

                std::ofstream o_file_exclusions;
                o_file_exclusions.open(HOME + FILE_EXCLUSIONS, std::ios::trunc);

                if(o_file_exclusions.is_open()) {
                    wordcount::renew_exclusion(o_file_exclusions, exclusions);
                    o_file_exclusions.close();
                
                    if(!unused.empty()) {
                        print_msg("Following word(s) were not processed:");
                        wordcount::print_exclusions(unused);
                    }

                    print_msg("Word(s) being excluded:");
                    wordcount::print_exclusions(exclusions);

                } else {
                    print_msg("Couldn't open exclusions file; no words were added or removed...\n");
                    return EXIT_FAILURE;
                }
                
                return EXIT_SUCCESS;
            }

            //Unknown argument will print usage.
            usage::print_usage();
            return EXIT_SUCCESS;
        }
    }

    std::ifstream file_text;
    file_text.open(HOME + FILE_WORDS);

    if(file_text.is_open()) {
        std::vector<wordcount::wd_count> words_v = wordcount::count_words(file_text, exclusions);
        wordcount::sort_words(words_v);

        if(TO_PRINT) wordcount::print_words(words_v, basewords_v);

        file_text.close();
    } else print_msg("Couldn't open text file. Exiting...\n");

    std::cout << "\nDone. Exiting..." << std::endl;
    return EXIT_SUCCESS;
}