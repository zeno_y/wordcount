#ifndef _WORDCOUNT_HPP
#define _WORDCOUNT_HPP

#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include <fstream>
#include <algorithm>
#include <limits>
#include <memory>

namespace wordcount {

// extern const char PUNCT[];
// extern const char DIVS[];

/**
 * word count class
 * 
 * Stores a word and the number of times it appears in any given text
 */
class wd_count {
//implicitly private
    int log = 0;
    std::string _word;
    int _count;
public:
    wd_count() : _word(""), _count(1) {if(log == 1) std::cout << "empty word ct'd @ " << this << std::endl;}
    wd_count(std::string word) : _word(word), _count(1) {if(log == 1) std::cout << "word " << _word << " ct'd @ " << this << std::endl;}
    wd_count(const wd_count & wordcount) : _word(wordcount.get_word()), _count(wordcount.get_count()) {if(log == 1) std::cout << "word " << _word << " copy ct'd @ " << this << " from " << &wordcount << std::endl;}

    ~wd_count() {if(log == 1) std::cout << "word " << _word << " dt'd @ " << this << std::endl;}

    inline const std::string & get_word() const {return _word;}
    inline void set_word(std::string word) {_word = word;}

    inline int get_count() const {return _count;}
    inline void set_count(int count) {_count = count;}

    bool operator < (const wd_count &) const; //used in std::sort. const safe
    bool operator == (const wd_count &) const; //used in wordcount::print_words. const safe
    wd_count & operator ++ (); //pre-increment
};

std::vector<std::string> get_exclusions(std::ifstream &);
void print_exclusions(const std::vector<std::string> &);
bool add_exclusion(std::vector<std::string> &, std::string &);
bool remove_exclusion(std::vector<std::string> &, std::string &);
void renew_exclusion(std::ofstream &, std::vector<std::string> &);

std::vector<wd_count> count_words(std::ifstream &, const std::vector<std::string> &);
void sort_words(std::vector<wd_count> &); //intentionally not const.
void print_words(const std::vector<wd_count> &, const std::vector<wd_count> &);

} //ends wordcount namespace

#endif //ends _WORDCOUNT_HPP if block