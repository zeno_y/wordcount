#include "usage.hpp"

//Formatting lengths.
#define FORMAT_ARG_SIZE 3 //ARG_SIZE + 1
#define FORMAT_ARG_USE 14
#define FORMAT_EXPL 21

/**
 * Formats the pass argument and usage. If usage is pointing to null, prints
 * a blank string of the same length.
 * 
 * * arg is of pointer type char and is the argument to format & print.
 * * use is of pointer type char and is the usage of the argument to format & print.
*/
void usage::format_usage(const char * arg, const char * use) { 
    std::cout << std::right << std::setw(FORMAT_ARG_SIZE) << arg << ' ';

    if(use != nullptr) std::cout << std::left << std::setw(FORMAT_ARG_USE) << use << ' ';
    else std::cout << std::string(FORMAT_ARG_USE + 1, ' '); //c-string.
}

/**
 * Formats the explanation given to an argument. If indent is true, indents the
 * c-string to vertically match the non-indented lines.
 * 
 * * indent is of type boolean and is true if the explanation requires an indent.
 * * expl is of pointer type char and is the explanation associated to the argument.
*/
void usage::format_explanation(bool indent, const char * expl) {
    if(!indent) std::cout << std::left << ": " << expl << std::endl;
    else std::cout << std::string(FORMAT_EXPL, ' ') << expl << std::endl; //c-string.
}

/**
 * Prints the usage of this program
*/
void usage::print_usage() {
    std::cout << "Usage: wdcount [option]" << std::endl;
    //explain file usage here
    std::cout << "Options: " << std::endl;
    
    usage::format_usage(ARG_HELP, nullptr);
    usage::format_explanation(false, "Prints out this message.");

    usage::format_usage(ARG_CMP, nullptr);
    usage::format_explanation(false, "Compares word count with base text.");

    usage::format_usage(ARG_LIST, nullptr);
    usage::format_explanation(false, "Lists all currently excluded words.");

    usage::format_usage(ARG_ADD, "word1 word2...");
    usage::format_explanation(false, "Adds words to exclusion list.");
    usage::format_explanation(true, "Separate desired words only by spaces.");
    
    usage::format_usage(ARG_REMOVE, "word1 word2...");
    usage::format_explanation(false, "Removes words from exclusion list.");
    usage::format_explanation(true, "Separate desired words only by spaces.");
}