#ifndef _USAGE_HPP
#define _USAGE_HPP

#include <iostream>
#include <iomanip>

//Defines argument size declared below
#define ARG_SIZE 2
//Defines expected position of argument
#define ARG_POS 1

//Valid arguments to pass with program
#define ARG_HELP "-h" //help text
#define ARG_CMP "-c" //compare with base text
#define ARG_ADD "-a" //add to excluded word(s) list
#define ARG_REMOVE "-r" //remove from excluded word(s) list
#define ARG_LIST "-l" //list exlusions

namespace usage {

void format_usage(const char * arg, const char * use);
void format_explanation(bool indent, const char * expl);
void print_usage();

}

#endif //ends _USAGE_HPP if block